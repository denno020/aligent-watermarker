/**
 * Resize the logo to be 1/4 the size of the uploaded image, and position it in the bottom right hand corner
 *
 * @param {canvas} image Uploaded image
 * @param {canvas} logo  Watermark logo
 *
 * @returns {*}
 */
function processImage(image, logo) {
  const imageContext = image.getContext('2d');

  const logoWidth = image.width / 2;
  const logoHeight = logoWidth / 1.26;

  imageContext.save();

  // Set the logo to 1/4 the size of the users uploaded image, and place it in the bottom right hand corner
  imageContext.drawImage(logo, image.width - logoWidth - 10, image.height - logoHeight - 10, logoWidth, logoHeight);

  imageContext.restore();

  return image;
}

const fileUploader = document.querySelector('input[type=file]');
fileUploader.onchange = (e) => {
  const input = e.target;
  document.querySelector('.preview').innerHTML = ''; // Clear any previous image
  document.querySelector('.loading-container').classList.toggle('is-loading', true);

  if (input.files.length === 0) {
    return;
  }

  const upload = input.files[0];

  const  fileType = upload['type'];
  const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
  if (!validImageTypes.includes(fileType)) {
    document.querySelector('.loading-container').classList.toggle('is-loading', false);
    alert('Please only select an image');
    return;
  }

  watermark([upload, '/img/watermark.svg'])
    .image(processImage)
    .then((img) => {
      document.querySelector('.preview').appendChild(img);
      document.querySelector('.loading-container').classList.toggle('is-loading', false);
    });
};
